umask u=rxw,g=rwx,o=

cd $EXECUTION_DIR

TMP_STDIN=/dev/null

cp $COMPRESS_LIST $DESTINATION_DIR
cp $COPY_LIST $DESTINATION_DIR 

FILE_LIST="$( find $DESTINATION_DIR -type f -printf "%p " )"

COMMAND="./run.sh --runtime_property DESTINATION_DIR:$DESTINATION_DIR --runtime_property REMOTE_USER:$REMOTE_USER --runtime_property REMOTE_HOST:$REMOTE_HOST --runtime_property PROJECT_NAME:$PROJECT_NAME --action setup_ssh_key --action transfer $FILE_LIST < $TMP_STDIN"

echo $COMMAND

./run.sh --runtime_property DESTINATION_DIR:$DESTINATION_DIR --runtime_property REMOTE_USER:$REMOTE_USER --runtime_property REMOTE_HOST:$REMOTE_HOST --runtime_property PROJECT_NAME:$PROJECT_NAME --action setup_ssh_key --action transfer $FILE_LIST < $TMP_STDIN

URL="$( grep 'Your Galaxy instance is waiting' $LOG | head -n 1 | sed s/'Your Galaxy instance is waiting at http\:\/\/'// )"
echo $URL > $URL_FILE

cd $START_DIR
