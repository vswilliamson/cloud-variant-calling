"""Fabric (http://docs.fabfile.org) deployment file to set up galaxy.
"""
# for Python 2.5
from __future__ import with_statement

import os
import time
import datetime as dt

# from fabric.api import *
# from fabric.contrib.files import *
from fabric.api import sudo, run, env, cd
from fabric.contrib.console import confirm
from fabric.contrib.files import exists, settings, hide, append
from fabric.colors import green, yellow, red

from util.fabric_helpers import vagrant, if_not_installed, if_installed, make_tmp_dir, ensure_can_sudo_into, setup_simple_service, start_service, chown_galaxy, setup_install_dir
from util.config import parse_options, get_galaxy_data

# -- Fabric instructions

def wait_for_galaxy():

    while not "8080" in run("netstat -lant"):
        # Check if galaxy has started                                                                   
        print "Waiting for galaxy to start."
        time.sleep(20)
        sudo("/etc/init.d/galaxy start")

def purge_galaxy():
    sudo("/etc/init.d/galaxy stop")
    sudo("rm -rf %s" % env.galaxy_home)
    sudo("/etc/init.d/postgresql-8.4 restart")
    sudo('psql  -c "drop database galaxy;"', user="postgres")
    sudo('psql  -c "create database galaxy;"', user="postgres")

def setup_galaxy(seed=True):
    """Deploy a Galaxy server along with some tools.
    """
    time_start = dt.datetime.utcnow()
    print(yellow("Configuring host '%s'. Start time: %s" % (env.hosts[0], time_start)))
    # Need to ensure the install dir exists and is owned by env.galaxy_user
    ensure_can_sudo_into("galaxy")
    setup_install_dir()
    _make_dir_for_galaxy("/mnt/galaxyDatabase")
    options = parse_options()
    target_galaxy_repo = options.get("Main", "target_galaxy_repo")
    _install_galaxy(target_galaxy_repo)

    options = parse_options()
    # TODO: Uncomment these.
    #_setup_indices()
    #_setup_tool_data()
    _fix_galaxy_permissions()
    _setup_shed_tools_dir()
    _setup_galaxy_log_dir()
    _migrate_galaxy_database()
    if seed:
        seed_database(options)
    _setup_galaxy_service()
    time_end = dt.datetime.utcnow()
    print(yellow("Duration of galaxy installation: %s" % str(time_end-time_start)))

def _migrate_galaxy_database():
    with cd(env.galaxy_home):
        sudo("bash -c 'export PYTHON_EGG_CACHE=eggs; ./create_db.sh -c universe_wsgi.ini.sample'", user="galaxy")

def seed_database(options):
    galaxy_data = get_galaxy_data()
    with cd(env.galaxy_home):
        sudo("rm -f seed.py")
        _setup_database_seed_file(options, galaxy_data)
        sudo("bash -c 'export PYTHON_EGG_CACHE=eggs; python seed.py -c universe_wsgi.ini.sample'", user="galaxy")

def seed_workflows(options):
    wait_for_galaxy()
    galaxy_data = get_galaxy_data()
    with cd(os.path.join(env.galaxy_home, "workflows")):
        for user in galaxy_data["users"]:
            api_key = user["api_key"]
            workflows = None
            if user.has_key("workflows"):
                workflows = user["workflows"]
            if not workflows:
                continue
            for workflow in workflows:
                sudo("bash -c 'export PYTHON_EGG_CACHE=eggs; bash import_all.sh %s %s'" % (api_key, workflow), user=env.galaxy_user)
        
def _setup_database_seed_file(options, galaxy_data):
    _seed_append("""from scripts.db_shell import *
from galaxy.util.bunch import Bunch
from galaxy.security import GalaxyRBACAgent
bunch = Bunch( **globals() )
bunch.engine = engine
# model.flush() has been removed. 
bunch.session = db_session
# For backward compatibility with "model.context.current"    
bunch.context = db_session
security_agent = GalaxyRBACAgent( bunch )
security_agent.sa_session = sa_session

def add_user(email, password, key=None):
    query = sa_session.query( User ).filter_by( email=email )
    if query.count() > 0:
        return query.first()
    else:
        user = User(email)
        user.set_password_cleartext(password)
        sa_session.add(user)
        sa_session.flush()

        security_agent.create_private_user_role( user ) 
        if not user.default_permissions:                           
            security_agent.user_set_default_permissions( user, history=True, dataset=True ) 

        if key is not None:
            api_key = APIKeys()
            api_key.user_id = user.id
            api_key.key = key
            sa_session.add(api_key)
            sa_session.flush()
        return user

def add_history(user, name):
    query = sa_session.query( History ).filter_by( user=user ).filter_by( name=name )
    if query.count() == 0:
        history = History(user=user, name=name)
        sa_session.add(history)
        sa_session.flush()
        return history
    else:
        return query.first()

""")
    i = 0
    for user in galaxy_data["users"]:
        username = user["username"]
        password = user["password"]
        api_key = user["api_key"]
        histories = None
        if user.has_key("histories"):
            histories = user["histories"]
        user_object = "user_%d" % i
        _seed_append("""%s = add_user("%s", "%s", "%s")""" % (user_object, username, password, api_key))
        _import_histories(user_object, histories)
        i = i + 1

def _import_histories(user_object, histories):
    if not histories:
        return
    for history_name in histories:
        _import_history(user_object, history_name)

def _import_history(user_object, history_name):
    history_name_stripped = history_name.strip()
    if history_name_stripped:
        _seed_append("""add_history(%s, "%s")""" % (user_object, history_name_stripped))

def _seed_append(text):
    append("%s/seed.py" % env.galaxy_home, text, use_sudo=True)


def _setup_galaxy_service():
    setup_simple_service("galaxy")
    # Create directory to store galaxy service's pid file.
    _make_dir_for_galaxy("/var/lib/galaxy")
    start_service("galaxy")

def refresh_galaxy(target_galaxy_repo):
    _update_galaxy(target_galaxy_repo)
    sudo("/etc/init.d/galaxy restart", pty=False)

def _setup_galaxy_log_dir():
    _make_dir_for_galaxy("/var/log/galaxy")

def _setup_shed_tools_dir():
    _make_dir_for_galaxy("%s/../shed_tools" % env.galaxy_home)
    
def _make_dir_for_galaxy(path):
    sudo("mkdir -p %s" % path)
    chown_galaxy(path)

def _fix_galaxy_permissions():
    # Ensure that everything under install dir is owned by env.galaxy_user
    chown_galaxy(os.path.split(env.install_dir)[0])
    sudo("chmod 755 %s" % os.path.split(env.install_dir)[0])

def _install_galaxy_fresh(target_galaxy_repo):
    sudo('mkdir -p %s' % env.galaxy_home)
    hg_command = "hg clone %s ." % target_galaxy_repo
    chown_galaxy(env.galaxy_home)
    with cd(env.galaxy_home):
        sudo(hg_command, user=env.galaxy_user)
    chown_galaxy(os.path.split(env.install_dir)[0])
    sudo("chmod 755 %s" % os.path.split(env.install_dir)[0])
    with cd(env.galaxy_home):
        # Make sure Galaxy runs in a new shell and does not inherit the environment      
        # by adding the '-ES' flag to all invocations of python within run.sh    
        sudo("sed -i 's/python .\//python -ES .\//g' run.sh", user=env.galaxy_user)
        # Set up the symlink for SAMTOOLS (remove this code once SAMTOOLS is converted to data tables)
    return True

def _update_galaxy(target_galaxy_repo):
    # Need to merge? -John
    hg_command = "hg pull %s; hg update" % target_galaxy_repo
    with cd(env.galaxy_home):
        sudo(hg_command, user=env.galaxy_user)
    
def _install_galaxy(target_galaxy_repo):
    """ Used to install Galaxy and setup its environment.
    This method cannot be used to update an existing instance of Galaxy code; see
    volume_manipulations_fab.py script for that functionality.
    Also, this method is somewhat targeted for the EC2 deployment so some tweaking
    of the code may be desirable."""
    sudo('mkdir -p %s' % env.galaxy_home)
    if exists(os.path.join(env.galaxy_home, ".hg")):
        _update_galaxy(target_galaxy_repo)
    else:
        _install_galaxy_fresh(target_galaxy_repo)

def _setup_indices():
    with cd(env.galaxy_home):
        if exists("%s/tool-data/sam_fa_indices.loc" % env.galaxy_home):
            sudo("rm %s/tool-data/sam_fa_indices.loc" % env.galaxy_home, user=env.galaxy_user)
        tmp_loc = False
	if not exists(env.galaxy_loc_files):
            sudo("mkdir -p %s" % env.galaxy_loc_files)
            sudo("chown -R %s:%s %s" % (env.galaxy_user, env.galaxy_user, env.galaxy_loc_files))
        if not exists("%s/sam_fa_indices.loc" % env.galaxy_loc_files):
            sudo("touch %s/sam_fa_indices.loc" % env.galaxy_loc_files, user=env.galaxy_user)
            tmp_loc = True
        sudo("ln -s %s/sam_fa_indices.loc %s/tool-data/sam_fa_indices.loc" % (env.galaxy_loc_files, env.galaxy_home), user=env.galaxy_user)
        if tmp_loc:
            sudo("rm %s/sam_fa_indices.loc" % env.galaxy_loc_files, user=env.galaxy_user)
    return True

def _setup_tool_data():
    with cd(env.galaxy_home):
        # set up the special HYPHY link in tool-data/
        hyphy_dir = os.path.join(env.install_dir, 'hyphy', 'default')
        sudo('ln -s %s tool-data/HYPHY' % hyphy_dir, user=env.galaxy_user)
        # set up the jars directory for Java tools
        if not exists('tool-data/shared/jars'):
            sudo("mkdir -p tool-data/shared/jars", user=env.galaxy_user)
        srma_dir = os.path.join(env.install_dir, 'srma', 'default')
        haploview_dir = os.path.join(env.install_dir, 'haploview', 'default')
        picard_dir = os.path.join(env.install_dir, 'picard', 'default')
        sudo('ln -s %s/srma.jar tool-data/shared/jars/.' % srma_dir, user=env.galaxy_user)
        sudo('ln -s %s/haploview.jar tool-data/shared/jars/.' % haploview_dir, user=env.galaxy_user)
        sudo('ln -s %s/*.jar tool-data/shared/jars/.' % picard_dir, user=env.galaxy_user)
    return True

