#!/bin/bash 

## Linux
lsof -p $$
lsof -p $$ | grep -E "/"$(basename $0)
LSOF=$(lsof -p $$ | grep -E "/"$(basename $0))
MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null | sed -r s/' \(.*\)$'//1 2>/dev/null )
if [ $? -ne 0 ]; then
## OSX
  MY_PATH=$(echo $LSOF | sed -E s/'^([^\/]+)\/'/'\/'/1 2>/dev/null )
fi
MY_DIR=$( dirname $MY_PATH )
EXECUTION_DIR=$MY_DIR/launcher
PROJECT_METADATA=$1
PROJECT_SOURCE_DIR=$(cd $(dirname $2); pwd)/$(basename $2)

COMMON_FILES="$( find $EXECUTION_DIR/common_files -type f -print0 | xargs -0 -n 1 printf "%s " )"
PROJECT_FILES="$( find $PROJECT_SOURCE_DIR -type f -print0 | xargs -0 -n 1 printf "%s " )"

DS="$( cut -f 3 $PROJECT_METADATA )"

DESTINATION_DIR="$MY_DIR/projects/$DS"
BG_SCRIPT="$DESTINATION_DIR/launcher.sh"
DATA_DIR="$DESTINATION_DIR/data"
umask u=rxw,g=rwx,o=
if [ ! -d $DATA_DIR ]
    then mkdir -p $DATA_DIR
fi
SAMPLE_SHEET=$DATA_DIR"/sample_sheet.tab"
sed s/\"//g $1 | sed s/\'//g | sed s/', '//g > "$SAMPLE_SHEET"

TO_EXIT=0
GENE_LIST=$( cut -f 4 $SAMPLE_SHEET | sed s/\"//g | sed s/,/" "/g )
for GENE in $GENE_LIST
do
        TOTAL=$( grep -c $GENE $EXECUTION_DIR/common_files/exons_2012_09_14.interval )
        if [ $TOTAL == '0' ]
        then
                echo $GENE" not found in "$EXECUTION_DIR"/common_files/exons_2012_09_14.interval"
                TO_EXIT=1
        fi
        TOTAL=$( grep -c $GENE $EXECUTION_DIR/common_files/exons_30bp_flank_2012_09_14.bed )
        if [ $TOTAL == '0' ]
        then
                echo $GENE" not found in "$EXECUTION_DIR"/common_files/exons_30bp_flank_2012_09_14.bed;"
                TO_EXIT=1
        fi
done

if [ $TO_EXIT != '0' ]
then
        echo "Errors detected in sample sheet gene list, exiting."
        exit -1
fi

echo "#!/bin/bash" > $BG_SCRIPT
echo "LOG=$DESTINATION_DIR/log" >> $BG_SCRIPT 
echo "START_DIR=\"\$( pwd )\"" >> $BG_SCRIPT
echo "EXECUTION_DIR="$( pwd )"/launcher" >> $BG_SCRIPT
echo "COPY_LIST=\"$COMMON_FILES\"" >> $BG_SCRIPT
echo "COMPRESS_LIST=\"$PROJECT_FILES\"" >> $BG_SCRIPT
echo "DESTINATION_DIR=$DATA_DIR" >> $BG_SCRIPT
echo "URL_FILE=$DESTINATION_DIR/hostname" >> $BG_SCRIPT
echo "REMOTE_USER="$( whoami ) >> $BG_SCRIPT
echo "REMOTE_HOST="$( hostname ) >> $BG_SCRIPT
echo "PROJECT_NAME=$DS" >> $BG_SCRIPT

cat $EXECUTION_DIR/templates/template.sh >> $BG_SCRIPT
chmod u+x $BG_SCRIPT

nohup $BG_SCRIPT > $DESTINATION_DIR/log 2>$DESTINATION_DIR/err &
