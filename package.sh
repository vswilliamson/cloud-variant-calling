#!/bin/bash 

## Linux
lsof -p $$
lsof -p $$ | grep -E "/"$(basename $0)
LSOF=$(lsof -p $$ | grep -E "/"$(basename $0))
#LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null | sed -r s/' \(.*\)$'//1 2>/dev/null )
if [ $? -ne 0 ]; then
## OSX
  MY_PATH=$(echo $LSOF | sed -E s/'^([^\/]+)\/'/'\/'/1 2>/dev/null )
fi
MY_DIR=$( dirname $MY_PATH )
EXECUTION_DIR=$MY_DIR/launcher

DS="package"
PROJECT_NAME=$DS
REMOTE_USER=$( whoami )
REMOTE_HOST=$( hostname )

DESTINATION_DIR="$MY_DIR/projects/$DS"
DATA_DIR="$DESTINATION_DIR/data"
umask u=rxw,g=rwx,o=
if [ ! -d $DATA_DIR ]
    then mkdir -p $DATA_DIR
fi

START_DIR=$( pwd )
EXECUTION_DIR=$START_DIR"/launcher"

cd $EXECUTION_DIR

TMP_STDIN=/dev/null

COMMAND="./run.sh --runtime_property DESTINATION_DIR:$DESTINATION_DIR --runtime_property REMOTE_USER:$REMOTE_USER --runtime_property REMOTE_HOST:$REMOTE_HOST --runtime_property PROJECT_NAME:$PROJECT_NAME --action setup_vm --action setup_galaxy --action setup_image --action setup_genomes --action setup_tools --action package"

echo $COMMAND

$COMMAND

cd $START_DIR
