# Based almost entirely on version from Dr. Enis Afgan at
# (https://bitbucket.org/afgane/mi-deployment)
import os, os.path, time, contextlib, tempfile, yaml, sys
import datetime as dt
from contextlib import contextmanager

from fabric.api import sudo, run, env, cd, put, local
from fabric.contrib.console import confirm
from fabric.contrib.files import exists, settings, hide, contains, append
from fabric.operations import reboot
from fabric.colors import red, green, yellow

from util.fabric_helpers import vagrant, if_not_installed, if_installed, make_tmp_dir, ensure_can_sudo_into, setup_simple_service, put_as_user, start_service
from util.shared import _yaml_to_packages

REPO_ROOT_URL = "https://bitbucket.org/jmchilton/mi-deployment/raw/tip"

# == Templates

xvfb_default_template = """XVFB_OPTS=":42 -auth /var/lib/xvfb/auth -ac -nolisten tcp -shmem -screen 0 800x600x24"\n"""
 
# -- Fabric instructions
def configure_MI():
    env.use_sudo = True
    env.install_dir = '/opt/galaxy/pkg'
    env.tmp_dir = "/mnt"
    env.shell = "/bin/bash -l -c"
    env.sources_file = "/etc/apt/sources.list"
    env.std_sources = ["deb http://watson.nci.nih.gov/cran_mirror/bin/linux/ubuntu lucid/"]

    time_start = dt.datetime.utcnow()
    print(yellow("Configuring host '%s'. Start time: %s" % (env.hosts[0], time_start)))
    _update_system()
    _install_packages()
    _setup_users()
    _required_programs()
    _configure_environment() 
    time_end = dt.datetime.utcnow()
    print(yellow("Duration of machine configuration: %s" % str(time_end-time_start)))

def _update_system():
    """Runs standard system update"""
    _setup_sources()
    with settings(warn_only=True): # Some custom CBL sources don't always work so avoid a crash in that case
        sudo('apt-get -y update')
        if env.package_holds:
            run('export DEBIAN_FRONTEND=noninteractive; sudo -E apt-get install wajig -y --force-yes')
            package_holds = env.package_holds.split(",")
            for package_hold in package_holds:
                run('wajig hold %s' % package_hold)
        run('export DEBIAN_FRONTEND=noninteractive; sudo -E apt-get upgrade -y --force-yes') # Ensure a completely noninteractive upgrade
        #sudo('apt-get -y dist-upgrade')
    print(yellow("Done updating the system"))

def _setup_sources():
    """Add sources for retrieving library packages."""
    for source in env.std_sources:
        if not contains(env.sources_file, source):
            append(env.sources_file, source, use_sudo=True)

def _apt_packages(pkgs_to_install):
    """Install packages available via apt-get.
    """
    # Disable prompts during install/upgrade of rabbitmq-server package
    sudo('echo "rabbitmq-server rabbitmq-server/upgrade_previous note" | debconf-set-selections')
    print(yellow("Update and install all packages"))
    _update_system() # Always update to ensure up-to-date mirrors
    # A single line install is much faster - note that there is a max
    # for the command line size, so we do 30 at a time
    group_size = 30
    i = 0
    print(yellow("Updating %i packages" % len(pkgs_to_install)))
    while i < len(pkgs_to_install):
        sudo("apt-get -y --force-yes install %s" % " ".join(pkgs_to_install[i:i+group_size]))
        i += group_size
    #sudo("apt-get clean")


def _install_packages(yaml_file=None):
    """ Get a list of packages to install base on the conf file and initiate
        installation of thos via apt-get.
    """
    def _read_main_config(yaml_file=None):
        """ Pull a list of groups to install based on the application configuration YAML.
            Reads 'applications.yaml' and returns a list of packages
        """
        if yaml_file is None:
            yaml_file = os.path.join('conf_files', "apps.yaml")
        with open(yaml_file) as in_handle:
            full_data = yaml.load(in_handle)
        print(yellow("Reading %s" % yaml_file))
        applications = full_data['applications']
        applications = applications if applications else []
        print(yellow("Applications whose packages to install: {0}".format(", ".join(applications))))
        return applications
    apps_to_install = _read_main_config(yaml_file)
    pkg_config_file = os.path.join('conf_files', "config.yaml")
    pkgs_to_install, _ = _yaml_to_packages(pkg_config_file, apps_to_install)
    _apt_packages(pkgs_to_install)
    print(green("----- Required system packages installed -----"))

def _setup_users():
    _add_user('galaxy', '1001') # Must specify uid for 'galaxy' user because of the configuration for proFTPd
    _add_user('postgres')
    _add_user('sgeadmin')

def _add_user(username, uid=None):
    """ Add user with username to the system """
    if not contains('/etc/passwd', "%s:" % username):
        print(yellow("System user '%s' not found; adding it now." % username))
        if uid:
            sudo('useradd -d /home/%s --create-home --shell /bin/bash -c"CloudMan-required user" --uid %s --user-group %s' % (username, uid, username))
        else:
            sudo('useradd -d /home/%s --create-home --shell /bin/bash -c"CloudMan-required user" --user-group %s' % (username, username))
        print(green("Added system user '%s'" % username))

# == required programs
def _required_programs():
    """ Install required programs """
    if not exists(env.install_dir):
        sudo("mkdir -p %s" % env.install_dir)
        sudo("chown %s %s" % (env.user, env.install_dir))
    
    # Setup global environment for all users
    install_dir = os.path.split(env.install_dir)[0]
    exports = [ "export PATH=%s/bin:%s/sbin:$PATH" % (install_dir, install_dir),
                "export LD_LIBRARY_PATH=%s/lib" % install_dir,
                "export DISPLAY=:42"]
    for e in exports:
        if not contains('/etc/bash.bashrc', e):
            append('/etc/bash.bashrc', e, use_sudo=True)
    # Install required programs
    _install_setuptools()
    _setup_nginx()
    _setup_postgresql()
    _install_samtools()

def _setup_nginx():
    _install_nginx()
    _configure_nginx()
    _configure_nginx_service()
    start_nginx()

def _nginx_install_dir():
    return os.path.join(env.install_dir, "nginx")

def _nginx_conf_dir():
    return os.path.join(_nginx_install_dir(), "conf")

def _configure_nginx():
    nginx_conf_file = 'nginx.conf'
    install_dir = _nginx_install_dir()
    remote_conf_dir = _nginx_conf_dir()
    url = os.path.join(REPO_ROOT_URL, nginx_conf_file)
    put("conf_files/nginx.conf", os.path.join(remote_conf_dir, nginx_conf_file), use_sudo=True)

    nginx_errdoc_file = 'nginx_errdoc.tar.gz'
    url = os.path.join(REPO_ROOT_URL, nginx_errdoc_file)
    remote_errdoc_dir = os.path.join(install_dir, "html")
    nginx_errdoc_tar_path = os.path.join(remote_errdoc_dir, nginx_errdoc_file)
    if exists(nginx_errdoc_tar_path):
        return
    with cd(remote_errdoc_dir):
        sudo("wget --output-document=%s %s" % (nginx_errdoc_tar_path, url))
        sudo('tar xvzf %s' % nginx_errdoc_file)

def _configure_nginx_service():
    setup_simple_service("nginx")
    
def start_nginx():
    start_service("nginx")

def _install_nginx():
    version = "0.7.67"
    upload_module_version = "2.0.12"
    upload_url = "http://www.grid.net.ru/nginx/download/" \
                 "nginx_upload_module-%s.tar.gz" % upload_module_version
    url = "http://nginx.org/download/nginx-%s.tar.gz" % version
    
    install_dir = _nginx_install_dir()
    remote_conf_dir = _nginx_conf_dir()
    
    # skip install if already present
    if exists(os.path.join(remote_conf_dir)):
        return
    
    with make_tmp_dir() as work_dir:
        with contextlib.nested(cd(work_dir), settings(hide('stdout'))):
            run("wget %s" % upload_url)
            run("tar -xvzpf %s" % os.path.split(upload_url)[1])
            run("wget %s" % url)
            run("tar xvzf %s" % os.path.split(url)[1])
            with cd("nginx-%s" % version):
                run("./configure --prefix=%s --with-ipv6 --add-module=../nginx_upload_module-%s --user=galaxy --group=galaxy --with-http_ssl_module --with-http_gzip_static_module" % (install_dir, upload_module_version))
                run("make")
                sudo("make install")
                with settings(warn_only=True):
                    sudo("cd %s; stow nginx" % env.install_dir)
    
    cloudman_default_dir = "/opt/galaxy/sbin"
    sudo("mkdir -p %s" % cloudman_default_dir)
    if not exists("%s/nginx" % cloudman_default_dir):
        sudo("ln -s %s/sbin/nginx %s/nginx" % (install_dir, cloudman_default_dir))
    print(green("----- nginx installed and configured -----"))

def _setup_postgresql():
    ensure_can_sudo_into("postgres")
    _configure_postgresql()
    _init_postgresql_data()

def _configure_postgresql(delete_main_dbcluster=False):
    """ This method is intended for cleaning up the installation when
    PostgreSQL is installed from a package. Basically, when PostgreSQL 
    is installed from a package, it creates a default database cluster 
    and splits the config file away from the data. 
    This method can delete the default database cluster that was automatically
    created when the package is installed. Deleting the main database cluster 
    also has the effect of stopping the auto-start of the postmaster server at 
    machine boot. The method adds all of the PostgreSQL commands to the PATH.
    """
    pg_ver = sudo("dpkg -s postgresql | grep Version | cut -f2 -d' ' | cut -f1 -d'-' | cut -f1-2 -d'.'")
    if delete_main_dbcluster:
        sudo('pg_dropcluster --stop %s main' % pg_ver, user='postgres')
    put_as_user("conf_files/postgresql.conf", "/etc/postgresql/8.4/main/postgresql.conf", user='root')
    exp = "export PATH=/usr/lib/postgresql/%s/bin:$PATH" % pg_ver
    if not contains('/etc/bash.bashrc', exp):
        append('/etc/bash.bashrc', exp, use_sudo=True)
    print(green("----- PostgreSQL configured -----"))

def _init_postgresql_data():
  ensure_can_sudo_into("postgres")

  if "galaxy" not in sudo("cat /var/lib/postgresql/8.4/main/global/pg_auth"):
      sudo("createdb galaxy", user="postgres")
      sudo("psql -c 'create user galaxy; grant all privileges on database galaxy to galaxy;'", user="postgres")

@if_not_installed("easy_install")
def _install_setuptools():
    version = "0.6c11"
    python_version = "2.6"
    url = "http://pypi.python.org/packages/%s/s/setuptools/setuptools-%s-py%s.egg#md5=bfa92100bd772d5a213eedd356d64086" % (python_version, version, python_version)
    with make_tmp_dir() as work_dir:
        with cd(work_dir):
            run("wget %s" % url)
            sudo("sh %s" % os.path.split(url)[1].split('#')[0])
            print(green("----- setuptools installed -----"))

# Why not in tools?
@if_not_installed("samtools")
def _install_samtools():
    version = "0.1.18"
    vext = ""
    mirror_info = "?use_mirror=cdnetworks-us-1"
    url = "http://downloads.sourceforge.net/project/samtools/samtools/%s/" \
            "samtools-%s%s.tar.bz2" % (version, version, vext)
    install_dir = "/usr/bin"
    install_cmd = sudo
    if not exists(install_dir):
        install_cmd("mkdir -p %s" % install_dir)
    with make_tmp_dir() as work_dir:
        with cd(work_dir):
            run("wget %s%s -O %s" % (url, mirror_info, os.path.split(url)[-1]))
            run("tar -xjvpf %s" % (os.path.split(url)[-1]))
            with cd("samtools-%s%s" % (version, vext)):
                run("sed -i.bak -r -e 's/-lcurses/-lncurses/g' Makefile")
                run("make")
                for install in ["samtools", "misc/maq2sam-long"]:
                    install_cmd("mv -f %s %s" % (install, install_dir))
                print "----- SAMtools %s installed to %s -----" % (version, install_dir)

# == environment

def _configure_environment():
    _configure_xvfb()

def _configure_xvfb():
    """Configure the virtual X framebuffer which is necessary for a couple tools."""
    setup_simple_service("xvfb")
    xvfb_default_file = 'conf_files/xvfb_default'
    with open( xvfb_default_file, 'w' ) as f:
        print >> f, xvfb_default_template
    remote_file = '/etc/default/xvfb'
    put_as_user(xvfb_default_file, remote_file, user='root')
    sudo("mkdir /var/lib/xvfb; chown root:root /var/lib/xvfb; chmod 0755 /var/lib/xvfb")
    print(green("----- configured xvfb -----"))

