

from fabric.api import local, run, sudo, env, put
from fabric.colors import green, yellow, red
from fabric.contrib.files import exists, settings, hide, append

from contextlib import contextmanager

import os

def vagrant():
    # change from the default user to 'vagrant'
    env.user = 'vagrant'
    # connect to the port-forwarded ssh
    env.hosts = ['127.0.0.1:2222']
 
    # use vagrant ssh key
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)
    print "Keyfile is %s" % result
    env.key_filename = result.split()[1]
    setup_environment()

def chown_galaxy(path):
    sudo("chown -R %s:%s %s" % (env.galaxy_user, env.galaxy_user, path))

def setup_install_dir():
    """Sets up install dir and ensures its owned by Galaxy"""
    if not exists(env.install_dir):
        sudo("mkdir -p %s" % env.install_dir)
    if not exists(env.jars_dir):
        sudo("mkdir -p %s" % env.jars_dir)
    chown_galaxy(os.path.split(env.install_dir)[0])
    

def ensure_can_sudo_into(user):
    sudoers_append("%admin  ALL = (" + user + ") NOPASSWD: ALL")

def sudoers_append(line):
    append("/etc/sudoers", line, use_sudo=True)

def start_service(service_name):
    # For reasons I don't understand this doesn't work for galaxy init
    # script unless pty=False
    sudo("/etc/init.d/%s start" % service_name, pty=False)

def setup_simple_service(service_name):
    init_file = "conf_files/%s_init" % service_name
    remote_file = "/etc/init.d/%s" % service_name
    put_as_user(init_file, remote_file, user="root", mode=755)
    sudo("ln -f -s /etc/init.d/%s /etc/rc0.d/K01%s" % (service_name, service_name))
    sudo("ln -f  -s /etc/init.d/%s /etc/rc1.d/K01%s" % (service_name, service_name))
    sudo("ln -f -s /etc/init.d/%s /etc/rc2.d/S99%s" % (service_name, service_name))
    sudo("ln -f -s /etc/init.d/%s /etc/rc3.d/S99%s" % (service_name, service_name))
    sudo("ln -f -s /etc/init.d/%s /etc/rc4.d/S99%s" % (service_name, service_name))
    sudo("ln -f -s /etc/init.d/%s /etc/rc5.d/S99%s" % (service_name, service_name))
    sudo("ln -f -s /etc/init.d/%s /etc/rc6.d/K01%s" % (service_name, service_name))


# -- Provide methods for easy switching between specific environment setups for 
# different deployment scenarios (an environment must be loaded as the first line
# in any invokable function)
def setup_environment():
    """Environment setup for Galaxy on Ubuntu 10.04
    Use this environment as a template 
    """
    env.keepalive = 30
    env.timeout = 60
    env.connection_attempts = 5
    env.galaxy_user = 'galaxy'
    env.data_files = '/mnt/galaxyIndices'
    env.galaxy_base = env.data_files + '/galaxy'
    env.tool_data_table_conf_file = "conf_files/tool_data_table_conf.xml"
    env.jars_dir = '/opt/galaxyTools/jars'
    env.picard_home = '%s/picard' % env.jars_dir
    env.install_dir = '/opt/galaxyTools/tools' # Install all tools under this dir
    env.galaxy_home = '/opt/galaxyTools/galaxy-central' # Where Galaxy is/will be installed
    env.galaxy_loc_files = '/mnt/galaxyIndices/galaxy/galaxy-data' # Where Galaxy's .loc files are stored
    env.update_default = True # If True, set the tool's `default` directory to point to the tool version currently being installed
    env.tmp_dir = "/mnt"
    env.shell = "/bin/bash -l -c"
    env.use_sudo = True

def if_tool_not_found():
    def argcatcher(func):
        def decorator(*args, **kwargs):
            package_name = func.__name__[len("_install_"):]
            print yellow("Checking the existence of %s" % package_name)
            env_file = os.path.join(env.install_dir, package_name, "default", "env.sh") 
            if not exists(env_file):                
                return func(*args, **kwargs)
            else:
                print green("Tool %s already appears to be installed (%s found), skipping." % (package_name, env_file))
        return decorator
    return argcatcher

def if_not_installed(pname): 
    def argcatcher(func):
        def decorator(*args, **kwargs):
            # with settings(hide('warnings', 'running', 'stdout', 'stderr'),
            #         warn_only=True):
            with settings(warn_only=True):
                result = run(pname)
            if result.return_code == 127:
                print(yellow("'%s' not installed; return code: '%s'" % (pname, result.return_code)))
                return func(*args, **kwargs)
            print(green("'%s' is already installed" % pname))
        return decorator
    return argcatcher

def if_installed(pname):
    """Run if the given program name is installed.
    """
    def argcatcher(func):
        def decorator(*args, **kwargs):
            with settings(
                    hide('warnings', 'running', 'stdout', 'stderr'),
                    warn_only=True):
                result = run(pname)
            if result.return_code in [0, 1]: 
                return func(*args, **kwargs)
        return decorator
    return argcatcher



@contextmanager
def make_tmp_dir():
    work_dir = os.path.join(env.tmp_dir, "tmp")
    if not exists(work_dir):
        sudo("mkdir %s" % work_dir)
        sudo("chown %s %s" % (env.user, work_dir))
    yield work_dir
    if exists(work_dir):
        print(red("About to delete work_dir %s" % work_dir))
        sudo("rm -rf %s" % work_dir)

def put_as_user(local_file, remote_file, user, mode=None):
    put(local_file, remote_file, use_sudo=True, mode=mode)
    sudo("chown %s:%s '%s'" % (user, user, remote_file))

