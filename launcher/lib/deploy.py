#!/usr/bin/env python

from libcloud.compute.base import NodeImage, NodeSize
from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.deployment import MultiStepDeployment, ScriptDeployment, SSHKeyDeployment
from libcloud.compute.types import NodeState    
from libcloud.compute.ssh import SSHClient
from libcloud.common.types import LibcloudError
from argparse import ArgumentParser
import os
import time
import gzip 
import shutil

import tempfile
from tempfile import tempdir
from glob import glob

from sys import exit
from threading import Thread
from threading import Condition
from Queue import Queue
from image import configure_MI
from tools import install_tools, purge_tools
from galaxy import setup_galaxy, refresh_galaxy, seed_database, seed_workflows, wait_for_galaxy, purge_galaxy
from genomes import install_data, install_data_s3
from util.config import parse_options, get_galaxy_data
from util.fabric_helpers import setup_environment, put_as_user, sudoers_append, chown_galaxy

from fabric.contrib.files import append
from fabric.api import local, put, run, env, settings, sudo, cd, get
from fabric.context_managers import prefix
from fabric.colors import red

from vmlauncher.transfer import FileTransferManager

CONFIG_MAIN_SECTION = 'Main'

# Ubuntu 10.04 LTS (Lucid Lynx) Daily Build [20120302]
DEFAULT_AWS_IMAGE_ID="ami-0bf6af4e"
DEFAULT_AWS_SIZE_ID="m1.large"
DEFAULT_AWS_AVAILABILITY_ZONE="us-west-1"




class VmLauncher:
    
    def __init__(self, options):
        self.options = options
        self.__set_and_verify_key()

    def __set_and_verify_key(self):
        self.key_file = self.options.get(CONFIG_MAIN_SECTION, 'key_file')
        if not os.path.exists(self.key_file):
	    self.key_file = os.getenv('HOME') + self.options.get(CONFIG_MAIN_SECTION, 'key_file')
	    if not os.path.exists(self.key_file):
                raise Exception, "Invalid or unspecified key_file options: %s" % self.key_file

    def get_key_file(self):
        return self.key_file

    def boot_and_connect(self):
        conn = self.connect_driver()
        node = self._boot() # Subclasses should implement this, and return libcloud node object
        self.conn = conn
        self.node = node
        self.connect(conn)

    def _wait_for_node_info(self, f):
        initial_value = f(self.node)
        if initial_value:
            return initial_value
        while True:
            time.sleep(10)
            refreshed_node = self._find_node()
            refreshed_value = f(refreshed_node)
            if refreshed_value:
                return refreshed_value

    def _find_node(self):
        nodes = self.conn.list_nodes()
        node_uuid = self.node.uuid
        for node in nodes:
            if node.uuid == node_uuid:
                return node


    def destroy(self, node=None):
        self.connect_driver()
        if node == None:
            node = self.node
        self.conn.destroy_node(node)

    def __get_ssh_client(self):
        ip = self.get_ip() # Subclasses should implement this
        key_file = self.get_key_file()
        print "Creating ssh client connection to ip %s" % ip
        ssh_client = SSHClient(hostname=ip, 
                               port=self.get_ssh_port(), 
                               username=self.get_user(), 
                               key=key_file)
        return ssh_client

    def get_user(self):
        return "ubuntu"

    def get_ssh_port(self):
        return 22

    def connect(self, conn, tries=5):
        i = 0
        while i < tries:
            try:
                ssh_client = self.__get_ssh_client()
                conn._ssh_client_connect(ssh_client=ssh_client,timeout=60)
                return
            except:
                i = i + 1 
            

    def list(self):
        self.connect_driver()
        return self.conn.list_nodes()

class VagrantConnection:
    """'Fake' connection type to mimic libcloud's but for Vagrant"""
    
    def _ssh_client_connect(self, ssh_client):
        pass

    def destroy_node(self, node=None):
        print "In destroy_node"
        local("vagrant halt")

    def list_nodes(self):
        return [VagrantNode()]


class VagrantNode:

    def __init__(self):
        self.name = "galaxy"
        self.uuid = "vagrant"

class VagrantVmLauncher(VmLauncher):
    """Launches vagrant VMs."""

    def connect_driver(self):
        self.conn = VagrantConnection()
        return self.conn

    def __init__(self, options):
        VmLauncher.__init__(self, options)
        self.uuid = "test"

    def _boot(self):
        local("vagrant up")
        return VagrantNode()

    def get_ip(self):
        return "33.33.33.11"

    def get_user(self):
        return "vagrant"

    def package(self):
        local("vagrant package")

class OpenstackVmLauncher(VmLauncher):
    """ Wrapper around libcloud's openstack API. """

    def __init__(self, options):
        VmLauncher.__init__(self, options)

    def get_ip(self):
        return self.public_ip

    def connect_driver(self):
        self.conn = self.__get_connection()
        return self.conn

    def _boot(self):
        conn = self.conn
        if self.options.has_option('Openstack', 'use_existing_instance'):
            instance_id = self.options.get('Openstack', 'use_existing_instance')
            nodes = conn.list_nodes()
            node = [node for node in nodes if node.uuid == instance_id][0]
        else:
            node = self.__boot_new(conn)
        return node

    def __boot_new(self, conn):
        if self.options.has_option('Openstack', 'image_id'):
            image_id = self.options.get('Openstack', 'image_id')
        else:
            image_id = None
        if self.options.has_option('Openstack', 'flavor_id'):
            flavor_id = self.options.get('Openstack', 'flavor_id')
        else:
            flavor_id = None
        key_name = self.options.get('Openstack', 'keypair_name')
        hostname = self.options.get(CONFIG_MAIN_SECTION, 'hostname')
        self.public_ip = self.options.get('Openstack', 'public_ip')

        images = conn.list_images()
        image = [image for image in images if (not image_id) or (image.id == image_id)][0]
        sizes = conn.list_sizes()
        size = [size for size in sizes if (not flavor_id) or (size.id == flavor_id)][0]
        
        node = conn.create_node(name=hostname, 
                                image=image, 
                                size=size, 
                                key_name=key_name)

        iteration_node = node
        while iteration_node.state is not NodeState.RUNNING:
            time.sleep(1)
            iteration_node = [n for n in conn.list_nodes() if n.uuid == node.uuid][0]

        conn.ex_add_floating_ip(node, self.public_ip)
        return node
    
    def __get_connection(self):
        driver = get_driver(Provider.OPENSTACK)
        openstack_api_host = self.options.get('Openstack', 'api_host')
        openstack_username = self.options.get('Openstack', 'username')
        openstack_api_key = self.options.get('Openstack', 'api_key')
        openstack_tennant_id = self.options.get('Openstack', 'tennant_id')

        auth_url = 'http://%s:5000' % openstack_api_host
        base_url = 'http://%s:8774/v1.1/%s/' % (openstack_api_host, openstack_tennant_id)
        conn = driver(openstack_username, 
                      openstack_api_key, 
                      False, 
                      host=openstack_api_host, 
                      port=8774,
                      ex_force_auth_url=auth_url,
                      ex_force_auth_version='1.0',
                      ex_force_base_url=base_url)
        return conn

class Ec2VmLauncher(VmLauncher):
    
    def __init__(self, options):
        VmLauncher.__init__(self, options)

    def get_ip(self):
        return self._wait_for_node_info(lambda node: node.extra['dns_name'])

    def connect_driver(self):
        self.conn = self.__get_connection()
        return self.conn

    def package(self):
        env.packaging_dir = "/mnt/packaging"
        sudo("mkdir -p %s" % env.packaging_dir)        
        self._copy_keys()
        self._install_ec2_tools()
        self._install_packaging_scripts()

    def _install_ec2_tools(self):
        sudo("apt-add-repository ppa:awstools-dev/awstools")
        sudo("apt-get update")
        sudo('export DEBIAN_FRONTEND=noninteractive; sudo -E apt-get install ec2-api-tools ec2-ami-tools -y --force-yes')
        
    def _install_packaging_scripts(self):
        user_id = self.options.get("AWS", "user_id")
        bundle_cmd="sudo ec2-bundle-vol -k %s/ec2_key -c%s/ec2_cert -u %s" % \
            (env.packaging_dir, env.packaging_dir, user_id)
        self._write_script("%s/bundle_image.sh" % env.packaging_dir, bundle_cmd)
        
        bucket = self.options.get("AWS", "package_bucket")
        upload_cmd="sudo ec2-upload-bundle -b %s -m /tmp/image.manifest.xml -a %s -s %s" % \
            (bucket,  self._access_id(), self._secret_key())
        self._write_script("%s/upload_bundle.sh" % env.packaging_dir, upload_cmd)

        name = self.options.get("AWS", "package_image_name")
        manifest = "image.manifest.xml"  
        register_cmd = "sudo ec2-register -K %s/ec2_key -C %s/ec2_cert %s/%s -n %s" % (env.packaging_dir, env.packaging_dir, bucket, manifest, name)
        self._write_script("%s/register_bundle.sh" % env.packaging_dir, register_cmd)


    def _write_script(self, path, contents):
        full_contents="#!/bin/bash\n%s" % contents
        sudo("echo '%s' > %s" % (contents, path))
        sudo("chmod +x %s" % path)

    def _copy_keys(self):
        ec2_key_path = self.options.get("AWS", "x509_key")
        ec2_cert_path = self.options.get("AWS", "x509_cert")
        put(ec2_key_path, "%s/ec2_key" % env.packaging_dir, use_sudo=True)
        put(ec2_cert_path, "%s/ec2_cert" % env.packaging_dir, use_sudo=True)

    def _boot(self):
        conn = self.conn
        if self.options.has_option('AWS', 'use_existing_instance'):
            instance_id = self.options.get('AWS', 'use_existing_instance')
            nodes = conn.list_nodes()
            for node in nodes:
                print node.uuid
                if node.uuid == instance_id:
                    return node
            raise Exception, "Could not find instance with id %s" % instance_id

        if self.options.has_option("AWS", "image_id"):            
            image_id = self.options.get("AWS", "image_id")
        else:
            image_id = DEFAULT_AWS_IMAGE_ID
            
        if self.options.has_option("AWS", "size_id"):
            size_id = self.options.get("AWS", "size_id")
        else:
            size_id = DEFAULT_AWS_SIZE_ID
        if self.options.has_option("AWS",  "availability_zone"):
            availability_zone = self.options.get("AWS", "availability_zone")
        else:
            availability_zone = DEFAULT_AWS_AVAILABILITY_ZONE

        image = NodeImage(id=image_id, name="", driver="")
        size = NodeSize(id=size_id, name="", ram=None, disk=None, bandwidth=None, price=None, driver="")
        locations = conn.list_locations()
        for location in locations:
            if location.availability_zone.name == availability_zone:
                break
        keyname = self.options.get("AWS", "keypair_name")
        hostname = self.options.get(CONFIG_MAIN_SECTION, "hostname")
        node = conn.create_node(name=hostname, 
                                image=image, 
                                size=size, 
                                location=location, 
                                ex_keyname=keyname)
        return node

    def __get_connection(self):
        driver = get_driver(Provider.EC2)
        ec2_access_id = self._access_id()
        ec2_secret_key = self._secret_key()
        conn = driver(ec2_access_id, ec2_secret_key)
        return conn

    def _access_id(self):
        return self.options.get("AWS", "ec2_access_id")

    def _secret_key(self):
        return self.options.get("AWS", "ec2_secret_key")


def build_vm_launcher(options):
    vm_host = options.get(CONFIG_MAIN_SECTION, 'vm_host')
    if vm_host and vm_host == 'openstack':
        vm_launcher = OpenstackVmLauncher(options)
    elif vm_host and vm_host == 'vagrant':
        vm_launcher = VagrantVmLauncher(options)
    else:
        vm_launcher = Ec2VmLauncher(options)
    return vm_launcher


def setup_fabric(vm_launcher, ip):
    env.user=vm_launcher.get_user()
    env.hosts=[ip]
    env.key_filename = vm_launcher.get_key_file()
    env.disable_known_hosts = True
    setup_environment()
        
def purge_genomes():
    sudo("rm -rf %s" % env.data_files)

def configure_smtp(options):
    if options.has_option(CONFIG_MAIN_SECTION, 'smtp_server'):
        smtp_server = options.get(CONFIG_MAIN_SECTION, 'smtp_server')
        username = options.get(CONFIG_MAIN_SECTION, 'smtp_user')
        password = options.get(CONFIG_MAIN_SECTION, 'smtp_password')
        conf_file_contents = """mailhub=%s
UseSTARTTLS=YES
AuthUser=%s
AuthPass=%s
FromLineOverride=YES
""" % (smtp_server, username, password)
        sudo("""echo "%s" > /etc/ssmtp/ssmtp.conf""" % conf_file_contents)
        aliases = """root:%s:%s
galaxy:%s:%s
%s:%s:%s""" % (username, smtp_server, username, smtp_server, env.user, username, smtp_server)
        sudo("""echo "%s" > /etc/ssmtp/revaliases""" % aliases)

def configure_sudoers(options):
    if options.has_option(CONFIG_MAIN_SECTION, "sudoers_addition"):
        for (name, value) in options.items(CONFIG_MAIN_SECTION):
            if name == "sudoers_addition":
                sudoers_append(value)

def configure_ssh_key(options):
    if options.has_option(CONFIG_MAIN_SECTION, "galaxy_ssh_key"):
        key_file = options.get(CONFIG_MAIN_SECTION, "galaxy_ssh_key")
        if key_file.find("~") == 0:
            key_file = key_file.replace("~", os.environ['HOME'], 1)
        sudo("mkdir -p /home/%s/.ssh" % (env.galaxy_user))
        sudo("chmod 700 /home/%s/.ssh" % (env.galaxy_user))
        put(local_path=key_file, 
            remote_path="/home/%s/.ssh/%s" % (env.galaxy_user, os.path.basename(key_file)),
            use_sudo=True,
            mode=0600)
        chown_galaxy("/home/%s/.ssh" % env.galaxy_user)
        

def configure_instance(options, actions):
    if "setup_image" in actions:
        if options.has_option(CONFIG_MAIN_SECTION, 'package_holds'):
            env.package_holds = options.get(CONFIG_MAIN_SECTION, 'package_holds')
        else:
            env.package_holds = None
        configure_MI()
        configure_smtp(options)
        configure_sudoers(options)
    if "purge_tools" in actions:
        purge_tools()
    if "setup_tools" in actions:
        install_tools()
    if "purge_genomes" in actions:
        purge_genomes()
    if "setup_genomes" in actions:
        install_proc = install_data
        sudo("mkdir -p %s" % env.data_files)
        sudo("mkdir -p %s/tool-data" % env.galaxy_base)
        sudo("chown -R %s:%s %s" % (env.user, env.user, env.data_files))
        put("conf_files/tool_data_table_conf.xml", "%s/tool_data_table_conf.xml" % env.galaxy_base)
        indexing_packages = ["bowtie", "bwa", "samtools"]
        path_extensions = ":".join(map(lambda package: "/opt/galaxy/pkg/%s/default" % package, indexing_packages))
        with prefix("PATH=$PATH:%s" % path_extensions):
            if 'S3' == options.get(CONFIG_MAIN_SECTION, 'genome_source'):
                install_proc = install_data_s3
            install_proc('conf_files/biodata.yaml', do_setup_environment=False)        
        stash_genomes_where = get_main_options_string(options, "stash_genomes")
        if stash_genomes_where:
            stash_genomes(stash_genomes_where)

    if "purge_galaxy" in actions:
        purge_galaxy()
    if "setup_galaxy" in actions:
        seed = _seed_at_configure_time(options)
        setup_galaxy(seed=seed)
        if seed:
            seed_workflows(options)
    if "setup_ssh_key" in actions:
        configure_ssh_key(options)

def _indices_dir_name():
    indices_dir = env.data_files
    if indices_dir.endswith("/"):
        indices_dir = indices_dir[0:(len(indices_dir)-1)]
    indices_dir_name = os.path.basename(indices_dir)
    return indices_dir_name


def _cd_indices_parent():
    return cd(_indices_parent())


def _indices_parent():
    parent_dir = os.path.abspath(os.path.join(env.data_files, ".."))
    return parent_dir


def stash_genomes(where):
    with _cd_indices_parent():
        sudo("chown %s:%s ." % (env.user, env.user))
        indices_dir_name = _indices_dir_name()
        remote_compressed_indices = "%s.tar.gz" % indices_dir_name
        run("tar czvf %s %s" % (remote_compressed_indices, indices_dir_name))
        if where == 'download':
            get(remote_path=remote_compressed_indices,
                local_path="compressed_genomes.tar.gz")
        elif where == 'opt':
            sudo("cp %s /opt/compressed_genomes.tar.gz" % remote_compressed_indices)
        else:
            print(red("Invalid option specified for stash_genomes [%s] - valid values include download and opt." % where))


def upload_genomes(options):
    with _cd_indices_parent():
        sudo("chown %s:%s ." % (env.user, env.user))
        indices_dir_name = _indices_dir_name()
        _transfer_genomes(options)
        run("rm -rf %s" % indices_dir_name)
        run("tar xzvfm compressed_genomes.tar.gz")
        sudo("/etc/init.d/galaxy restart")


def parse_args():
    parser = ArgumentParser("Creates an on-demand Galaxy instance and transfers files to it.")
    parser.add_argument('--action', action="append", default=[])
    parser.add_argument('--runtime_property', action="append", default=[])
    parser.add_argument('--compressed_file', dest="compressed_files", action="append", default=[])
    parser.add_argument('files', metavar='file', nargs='*',
                        help='files to upload to Galaxy')
    args = parser.parse_args()
    print args
    if len(args.action) == 0:
        args.action = ["transfer"]
    return args


def transfer_files(options, args):
    transfer_options = _build_transfer_options(options, "/mnt/uploaded_data", "galaxy")
    _do_transfer(transfer_options, args.files, args.compressed_files)
    shutil.rmtree(transfer_options['local_temp'])

def _transfer_genomes(options):
    transfer_options = _build_transfer_options(options, _indices_parent(), env.user)
    transfer_options["compress"] = False
    _do_transfer(transfer_options, ["compressed_genomes.tar.gz"])
    shutil.rmtree(transfer_options['local_temp'])


def _build_transfer_options(options, destination, user):
    transfer_options = {}
    transfer_options['compress'] = get_boolean_option(options, CONFIG_MAIN_SECTION, 'compress_transfers', True)
    transfer_options['num_compress_threads'] = int(get_main_options_string(options, 'num_compress_threads', '1'))
    transfer_options['num_transfer_threads'] = int(get_main_options_string(options, 'num_transfer_threads', '1'))
    transfer_options['num_decompress_threads'] = int(get_main_options_string(options, 'num_decompress_threads', '1'))
    transfer_options['chunk_size'] = int(get_main_options_string(options, 'transfer_chunk_size', '0'))
    transfer_options['transfer_retries'] = int(get_main_options_string(options, 'transfer_retries', '3'))
    transfer_options['local_temp'] = tempfile.mkdtemp(dir=get_main_options_string(options, 'local_temp_dir', tempdir))
    transfer_options['destination'] = destination
    transfer_options['transfer_as'] = user
    return transfer_options


def _do_transfer(transfer_options, files, compressed_files=[]):
    FileTransferManager(**transfer_options).transfer_files(files, compressed_files)

def get_boolean_option(options, section, name, default=False):
    if not options.has_option(section, name):
        return default
    else:
        return options.getboolean(section, name)

def get_main_options_string(options, key, default=''):
    value = default
    if options.has_option(CONFIG_MAIN_SECTION, key):
        value = options.get(CONFIG_MAIN_SECTION, key)
    return value

def create_data_library_for_uploads(options):
    with cd(os.path.join(env.galaxy_home, "scripts", "api")):
        db_key_arg = get_main_options_string(options, 'db_key')
        transfer_history_name = get_main_options_string(options, 'transfer_history_name')
        transfer_history_api_key = get_main_options_string(options, 'transfer_history_api_key')
        cmd_template = 'python handle_uploads.py --api_key="%s" --db_key="%s" --history="%s" --history_api_key="%s" '
        galaxy_data = get_galaxy_data()
        admin_user_api_key = galaxy_data["users"][0]["api_key"]
        cmd = cmd_template % (admin_user_api_key, db_key_arg, transfer_history_name, transfer_history_api_key)
        sudo("bash -c 'export PYTHON_EGG_CACHE=eggs; %s'" % cmd, user="galaxy")
                
def copy_runtime_properties(fqdn, args):
    runtime_properties_raw = args.runtime_property
    runtime_properties = { "FQDN" : fqdn }
    for runtime_property_raw in runtime_properties_raw:
        (name, value) = runtime_property_raw.split(":")
        runtime_properties[name] = value
    export_file = ""
    for (name, value) in runtime_properties.iteritems():
        export_file = "export %s=%s\n%s" % (name, value, export_file)
    sudo('mkdir -p %s' % env.galaxy_home)
    chown_galaxy(env.galaxy_home)
    sudo("echo '%s' > %s/runtime_properties" % (export_file, env.galaxy_home), user=env.galaxy_user)

def setup_vm(options, args, vm_launcher, actions):
    try:
        ip = vm_launcher.get_ip()
        setup_fabric(vm_launcher, ip)
        with settings(host_string=ip):
            if options.has_option(CONFIG_MAIN_SECTION, 'max_lifetime'):
                seconds = options.getint(CONFIG_MAIN_SECTION, 'max_lifetime')
                # Unclear why the sleep is needed, but seems to be otherwise 
                # this doesn't work.
                run("bash -c 'nohup sudo shutdown -h %d &'; sleep 2" % seconds)
            configure_instance(options, actions)        
            do_refresh_galaxy = get_boolean_option(options, CONFIG_MAIN_SECTION, 'refresh_galaxy', False)
            do_upload_genomes = get_boolean_option(options, CONFIG_MAIN_SECTION, 'upload_genomes', False)
            if do_refresh_galaxy:
                refresh_galaxy(options.get(CONFIG_MAIN_SECTION, 'target_galaxy_repo'))
            copy_runtime_properties(ip, args)    
            if 'transfer' in actions:
                transfer_files(options, args)
                if do_upload_genomes:
                    upload_genomes(options)
            if not _seed_at_configure_time(options):
                seed_database()
                seed_workflows(options)
            if 'transfer' in actions:
                wait_for_galaxy()
                create_data_library_for_uploads(options)
                print 'Your Galaxy instance is waiting at http://%s' % ip    
            if 'package' in actions:
		print 'Running packaging'
                vm_launcher.package()
    finally:
        if get_boolean_option(options, CONFIG_MAIN_SECTION, 'destroy_on_complete', False):
            vm_launcher.destroy()

def _seed_at_configure_time(options):
    if options.has_option(CONFIG_MAIN_SECTION, 'seed_galaxy'):
        return options.get(CONFIG_MAIN_SECTION, 'seed_galaxy') == 'configure'
    else:
        return True

def expand_actions(actions):
    unique_actions = set()
    for simple_action in ["list", "destroy", "transfer",
                          "purge_galaxy", "setup_galaxy", 
                          "purge_tools", "setup_tools", 
                          "purge_genomes", "setup_genomes",
                          "setup_ssh_key",
                          "package",
                          "setup_image"]:
        if simple_action in actions:
            unique_actions.add(simple_action)
    compound_actions = {"configure": ["setup_image", "setup_tools", "setup_genomes", "setup_galaxy", "setup_ssh_key"],
                        "reinstall_galaxy": ["purge_galaxy", "setup_galaxy"],
                        "reinstall_genomes": ["purge_genomes", "setup_genomes"],
                        "reinstall_tools": ["purge_tools", "setup_tools"]}
    for compound_action in compound_actions.keys():
        if compound_action in actions:
            for compound_action_part in compound_actions[compound_action]:
                unique_actions.add(compound_action_part)
    return unique_actions

def main():
    options = parse_options()
    args = parse_args()
    actions = expand_actions(args.action)
    vm_launcher = build_vm_launcher(options)

    if "list" in actions:
        actions.remove("list")
        for node in vm_launcher.list():
            print "Active node with uuid %s %s " % (node.uuid, node)

    if "destroy" in actions:
        actions.remove("destroy")
        target_name = options.get(CONFIG_MAIN_SECTION, "hostname")
        for node in vm_launcher.list():
            node_name = node.name
            print "comp %s to %s" % (target_name, node_name)
            if node_name == target_name:
                vm_launcher.destroy(node)

    # Do we have remaining actions requiring boot a Galaxy instance.
    if len(actions) > 0:
        print 'Setting up virtual machine for Galaxy'
        vm_launcher.boot_and_connect()
        setup_vm(options, args, vm_launcher, actions)

if __name__ == "__main__":
    main()
