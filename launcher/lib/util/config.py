from ConfigParser import ConfigParser
import inspect, os
import yaml

def path_from_root(name):
  root_path = os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), "..", "..")
  file_path = os.path.join(root_path, name)
  return file_path

def parse_options(name="options"):
  config = ConfigParser()
  options_path = path_from_root("%s.ini" % name)
  config.readfp(open(options_path))
  return config

def read_yaml(yaml_file):
  with open(yaml_file) as in_handle:
    return yaml.load(in_handle)

def get_galaxy_data():
  yaml_path = path_from_root("galaxy_data.yaml")
  return read_yaml(yaml_path)
